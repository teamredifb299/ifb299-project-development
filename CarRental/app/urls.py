from django.conf.urls import url, include
from django.contrib import admin

from app.views import customerPageViews, accountManagementViews, customerToolViews, customerSearchView, vehicleReportingViews, inspectionView, storeDataViews

urlpatterns = [
    url(r'^about/', customerSearchView.About, name = 'about'),
    url(r'^home/', customerSearchView.Home, name = 'home'),
    url(r'^login/', accountManagementViews.Login, name = 'login'),
    url(r'^logout/', accountManagementViews.Logout, name = 'logout'),
    url(r'^register/', accountManagementViews.Register, name = 'register'),
    url(r'^register_success/', accountManagementViews.RegisterSuccess, name = 'registerSuccess'),
    url(r'^CustomerTools/search/', customerToolViews.Search, name = 'customerSearch'),
    url(r'CustomerTools/createCustomer', customerToolViews.CreateCustomer, name = 'createCustomer'),
    url(r'^CustomerTools/CarSearch/(?P<customerID>[0-9]+)/$', customerToolViews.SearchCar),
    url(r'^CustomerTools/CreateRental/(?P<customer_id>[0-9]+)/$', customerToolViews.CreateRental),
    url(r'CustomerTools/UpdateCustomerInfo', customerToolViews.UpdateCustomerInfo),
    url(r'CustomerTools/customerInspectionReport', inspectionView.ViewCustomerInspectionReport),
    url(r'CustomerTools/ViewRentalStatus', customerToolViews.ViewRental, name = 'RentalStatusPage'),
    url(r'CustomerTools/StoreInformation', customerToolViews.StoreInformation, name = 'StoreInformation'),
    url(r'CustomerTools/inspectionReport/submitted', inspectionView.inspection_added),
    url(r'CustomerTools/inspectionReport', inspectionView.submit_inspection),
    url(r'CustomerTools/StaffPortal', customerToolViews.staffPortal),
    url(r'Reporting/VehiclePerformance', vehicleReportingViews.VehiclePerformancePage),
    url(r'Reporting/ajax/SummaryTable', vehicleReportingViews.SummaryTable, name = "AjaxSummaryTable"),
    url(r'Reporting/ajax/Search', vehicleReportingViews.Search, name = "AjaxSearch"),
    url(r'Reporting/ajax/ModelNameUpdate', vehicleReportingViews.ModelNameUpdate, name = "AjaxModelNameUpdate"),
    url(r'Reporting/ajax/ModelSeriesUpdate', vehicleReportingViews.ModelSeriesUpdate, name = "AjaxSeriesUpdate"),
    url(r'Reporting/CarSummaryTool', vehicleReportingViews.CarSummary, name = "VehicleDetailsPage"),
    url(r'CustomerTools/createModel', customerToolViews.CreateModel, name = 'createModel'),
    url(r'CustomerTools/ModelAdded', customerToolViews.ModelAdded, name = 'ModelAdded'),
    url(r'Reporting/BranchPerformance', storeDataViews.CustomerPerformancePage, name ='BranchPerformance'),
    url(r'Reporting/ajax/BranchSummaryTable', storeDataViews.CustomerSummaryTable, name = "AjaxCustomerSummaryTable"),
    url(r'customer', customerPageViews.CustomerPage, name = 'customerPage'),
    url(r'CustomerPage/returns', customerPageViews.closedRentalsFilter, name = 'ClosedRentalsTable'),
    url(r'CustomerPage/pickups', customerPageViews.activeRentalsFilter, name = 'ActiveRentalsTable'),
    url(r'CustomerPage/returnCar', customerPageViews.ReturnCar, name='ReturnCar'),
    url(r'CustomerTools/createCar', customerToolViews.CreateCar, name = 'createCar'),

]

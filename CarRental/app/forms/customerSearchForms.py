from django import forms
from app.models import Car_Model, Car

class Customer_Search_Car_Form(forms.ModelForm):

    TRANSMISSION_CHOICES = (
        ('', 'Any'),
        ('A', 'Automatic'),
        ('M', 'Manual'),
    )

    dailyRate = forms.CharField(required=False)
    make = forms.CharField(required=False)
    modelName = forms.CharField(required=False)
    series = forms.CharField(required=False)
    seatingCapacity = forms.IntegerField(required=False)
    standardTransmission = forms.CharField(widget=forms.Select(choices=TRANSMISSION_CHOICES), required=False)
	
    class Meta:
        model = Car_Model
        fields = ('dailyRate', 'make', 'modelName', 'series', 'seatingCapacity', 'standardTransmission',)

class Customer_Search_Car_Store_Form(forms.ModelForm):

    STORE_CHOICES = (
        ('', 'Any Store:'),
        ('', 'Queensland'),
        ('20', '-- Brisbane'),
        ('21', '-- Caloundra'),
        ('22', '-- East Brisbane'),
        ('23', '-- Gold Coast'),
        ('24', '-- Hawthorne'),
        ('25', '-- Hervey Bay'),
        ('26', '-- Rockhampton'),
        ('27', '-- Townsville'),
        ('', 'New South Wales'),
        ('1', '-- Alexandria'),
        ('2', '-- Coffs Harbour'),
        ('3', '-- Darlinghurst'),
        ('4', '-- Goulburn'),
        ('5', '-- Lane Cove'),
        ('6', '-- Lavender Bay'),
        ('7', '-- Malabar'),
        ('8', '-- Matraville'),
        ('9', '-- Milsons Point'),
        ('10', '-- Newcastle'),
        ('11', '-- North Ryde'),
        ('12', '-- North Sydney'),
        ('13', '-- Port Macquarie'),
        ('14', '-- Rhodes'),
        ('15', '-- Silverwater'),
        ('16', '-- Springwood'),
        ('17', '-- St. Leonards'),
        ('18', '-- Sydney'),
        ('19', '-- Wollongong'),
        ('', 'Victoria'),
        ('32', '-- Bendigo'),
        ('33', '-- Cranbourne'),
        ('34', '-- Geelong'),
        ('35', '-- Melbourne'),
        ('36', '-- Melton'),
        ('', 'Tasmania'),
        ('31', '-- Hobart'),
        ('', 'South Australia'),
        ('29', '-- Findon'),
        ('', 'Western Australia'),
        ('28', '-- Cloverdale'),
        ('30', '-- Perth'),
        ('37', '-- Seaford'),
        ('38', '-- South Melbourne'),
        ('39', '-- Sunbury'),
        ('40', '-- Warrnambool'),
    )
    
    store_id = forms.CharField(widget=forms.Select(choices=STORE_CHOICES), required=False)
    
    class Meta:
        model = Car
        fields = ('store_id',)
from django import forms
from app.models import Inspection

class submit_inspection(forms.Form):
    carID = forms.IntegerField(label="Car Number", required=True)

class create_inspection(forms.ModelForm):
    #comments = forms.CharField( widget=form.TextArea )

    class Meta:
        model = Inspection
        fields = ('inspectionID', 'comments', 'dateCompleted')
        labels = {
            'dateCompleted': 'Date Completed',
        }
        widgets = {
            'comments': forms.Textarea,
        }

from django import forms
from app.models import Account, Customer, Car_Model, Car

class Login_Form(forms.ModelForm):
    class Meta:
        model = Account
        fields = ('username', 'password')
        widgets = {
            'password': forms.PasswordInput(),
        }

class Register_Form(forms.ModelForm):
    class Meta:
        model = Customer
        fields = ('userID', 'username', 'password', 'name', 'dob', 'phone', 'address', 'email', 'occupation', 'gender', 'license')
        widgets = {
            'password': forms.PasswordInput(),
        }

class Model_Register_Form(forms.ModelForm):
    class Meta:
        model = Car_Model
        fields = ('modelID', 'make', 'modelName', 'series', 'seriesYear', 'priceNew', 'engineSize', 'fuelSystem', 'tankCapacity', 'power', 'seatingCapacity', 'standardTransmission', 'bodyType', 'drive', 'wheelBase', 'imgSource', 'dailyRate')
        widgets = {
            'password': forms.PasswordInput(),
        }

class Car_Register_Form(forms.ModelForm):
    class Meta:
        model = Car
        fields= ('carID', 'carStatus', 'model','store')
        widgets = {
            'password': forms.PasswordInput(),
        }
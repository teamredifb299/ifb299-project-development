from django import forms
from app.models import Customer, Car_Model

class Update_Customer_Info(forms.ModelForm):
    class Meta:
        phone = forms.IntegerField(initial='customer.phone')
        model = Customer
        fields = ('name', 'dob', 'phone', 'address', 'email', 'occupation', 'gender', 'license')

class Search_Customer_Info(forms.ModelForm):
    name = forms.CharField(required=False)
    phone = forms.IntegerField(required=False)
    license = forms.IntegerField(required=False)
    class Meta:
        model = Customer
        fields = ('name', 'phone', 'license')


class Search_Car_Form(forms.ModelForm):
    DRIVE_CHOICES = (
        ('', 'Any'),
        ('4WD', 'Four Wheel Drive'),
        ('AWD', 'All Wheel Drive'),
        ('FWD', 'Front Wheel Drive'),
        ('RWD', 'Rear Wheel Drive'),
    )

    make = forms.CharField(required=False)
    series = forms.CharField(required=False)
    seriesYear = forms.IntegerField(required=False)
    price = forms.IntegerField(required=False)
    engineSize = forms.CharField(required=False)
    fuelSystem = forms.CharField(required=False)
    tankCapacity = forms.CharField(required=False)
    power = forms.CharField(required=False)
    seatingCapacity = forms.CharField(required=False)
    standardTransmission = forms.CharField(required=False)
    bodyType = forms.CharField(required=False)
    drive = forms.CharField(widget=forms.Select(choices=DRIVE_CHOICES), required=False)
    wheelBase = forms.CharField(required=False)

    class Meta:
        model = Car_Model
        fields = ('make', 'series', 'seriesYear',
            'engineSize', 'fuelSystem', 'tankCapacity', 'power',
            'seatingCapacity', 'standardTransmission', 'bodyType',
            'drive', 'wheelBase',) + ('price',)

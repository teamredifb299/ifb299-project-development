from django.db import models, connection
from passlib.hash import sha256_crypt
import datetime
import dateutil.parser

#This class provides the functionality to authenticate users when trying to login and determine their access levels
#Authors:
    #   Jonathan Poulton n9731008
class User_Authentication():

    CUSTOMER_ROLE = 'Customer'
    STAFF_ROLE = 'Staff'
    MANAGER_ROLE = 'Manager'
    MECHANIC_ROLE = 'Mechanic'

    #This method checks if the session has a user loggedin
    #Authors:
    #   Jonathan Poulton n9731008
    def LoginStatus(request):
        try:
            loggedInStatus = request.session['loggedIn']
        except Exception as e:
            loggedInStatus = False
        return loggedInStatus

    #This method checks if the user currently logged user's access meets the requirements for a page
    #Authors:
    #   Jonathan Poulton n9731008
    def AccessLevelMet(request, requiredLevel):
        account_id = request.session['user']
        account = Account.objects.get(userID=account_id)

        #Checks what type of access is required
        if requiredLevel == User_Authentication.STAFF_ROLE:
            accessMet = account.AccessRequiredStaff()
        elif requiredLevel == User_Authentication.MANAGER_ROLE:
            accessMet = account.AccessRequiredManager()
        elif requiredLevel == User_Authentication.MECHANIC_ROLE:
            accessMet = account.AccessRequiredMechanic()
        else:
            accessMet = True
        return accessMet

    #This method checks if the username and password match an account stored in the database
    #Authors:
    #   Jonathan Poulton n9731008
    def Login(username, password):
        user = Account.objects.GetUser(username)
        if not user == False:
            verified = user.VerifyPassword(password)
            if verified:
                return user
            else:
                return False
        else:
            return False

#This class allows access to the whole data set of Accounts
#Authors:
    #   Jonathan Poulton n9731008
class Account_Manager(models.Manager):

    #This method tries to find a user based off the password
    #This Method was created by Jonathan Poulton n9731008
    def GetUser(self, username):
        try:
            user = self.get(username=username)
        except Exception as e:
            user = False
        return user

#This class implements the Account model, tracking a user's details so that login
#functionality can be implemented
#Authors:
    #   Jonathan Poulton n9731008
class Account(models.Model):
    userID = models.AutoField(primary_key=True)
    password = models.CharField(max_length=500)
    username = models.CharField(max_length=75, unique=True)
    role = models.ForeignKey('Role', models.DO_NOTHING, default=1)

    objects = Account_Manager()

    #This method sets the name displayed when converting the object to a string
    def __str__(self):
        return self.username

    #This method encrypts the user's password so it can be stored securly
    #Authors:
    #   Jonathan Poulton n9731008
    def save(self, force_insert=False, using=None):
        self.password = sha256_crypt.encrypt(self.password)

        super(Account, self).save()

    #This method checks that the password passed into it is the origin of the encrypted password
    #Authors:
    #   Jonathan Poulton n9731008
    def VerifyPassword(self, enteredPassword):
        return sha256_crypt.verify(enteredPassword ,self.password)

    #This method checks that the user has the access level of a staff member
    #Authors:
    #   Jonathan Poulton n9731008
    def AccessRequiredStaff(self):
        userLevel = self.role.name
        if userLevel == 'Staff' or userLevel =='Manager':
            return True
        else:
            return False

    #This method checks if the user has the access level of a manager
    #Authors:
    #   Jonathan Poulton n9731008
    def AccessRequiredManager(self):
        userLevel = self.role.name
        if userLevel =='Manager':
            return True
        else:
            return False

    class Meta:
        db_table = 'accounts'

    #This method checks if the user has the access level of a mechanic
    #Authors:
    #   Matthew Keye n9697403
    def AccessRequiredMechanic(self):
        userLevel = self.role.name
        if userLevel =='Mechanic':
            return True
        else:
            return False

    class Meta:
        db_table = 'accounts'

# class Role_Manager(models.Manager):

#This class allows accounts to be assigned an access type
#Authors:
    #   Jonathan Poulton n9731008
class Role(models.Model):
    roleID = models.AutoField(primary_key=True)
    name = models.CharField(max_length=20)

    # objects.Role_Manager()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'roles'

# class Customer_Manager(models.Manager):
#Authors:
    #   Jonathan Poulton n9731008
class Customer(Account):
    name = models.CharField(max_length=75)
    dob = models.DateField()
    phone = models.CharField(max_length=20)
    address = models.CharField(max_length=75)
    email = models.CharField(max_length=75)
    occupation = models.CharField(max_length=75)
    gender = models.CharField(max_length=13)
    license = models.IntegerField()

    # objects = Customer_Manager()

    def __str__(self):
        return self.name

    class Meta:
        db_table = 'customers'

# class Staff_Manager(models.Manager):

#This class allows for staff to be stored
#It extends the Account model as they have the same methods and attributes but acocunt variables
#need to be stored in a separate model so that there is a link with customers
#Authors:
    #   Jonathan Poulton n9731008
class Staff(Account):
    staffName = models.CharField(max_length=75)
    store = models.ForeignKey('Store', models.DO_NOTHING)

    # objects = Staff_Manager()

    def __str__(self):
        return self.staffName

    class Meta:
        db_table = 'staff'

#This class allows for the dataset for Car Models to be access, primarily searching functionality
#Authors:
    #   Jonathan Poulton n9731008
class Model_Manager(models.Manager):
    #This method searches over the data set of car models and finds models that match the form entries
    #Authors:
    #   Jonathan Poulton n9731008
	#	Jacob McDonald n8616469
    def SearchModels(self, searchForm):
        #Extract variables form the form
        make = searchForm['make'].value()
        series = searchForm['series'].value()
        seriesYear= searchForm['seriesYear'].value()
        price = searchForm['price'].value()
        engineSize = searchForm['engineSize'].value()
        fuelSystem = searchForm['fuelSystem'].value()
        tankCapacity = searchForm['tankCapacity'].value()
        power = searchForm['power'].value()
        seatingCapacity = searchForm['seatingCapacity'].value()
        standardTransmission = searchForm['standardTransmission'].value()
        bodyType = searchForm['bodyType'].value()
        drive = searchForm['drive'].value()
        wheelBase = searchForm['wheelBase'].value()

        #Filter all vehicles based upon whether or not the meet the search critera
        vehicles = self.filter(make__icontains=make,series__icontains=series, tankCapacity__icontains=tankCapacity,
            seriesYear__icontains=seriesYear, priceNew__icontains=price, seatingCapacity__icontains=seatingCapacity,
			standardTransmission__icontains=standardTransmission, engineSize__icontains=engineSize, fuelSystem__icontains=fuelSystem,
			power__icontains=power, bodyType__icontains=bodyType, drive__icontains=drive, wheelBase__icontains=wheelBase)

        #Return vehicles
        return vehicles
    #This method searches over the data set of car models and finds models that match the form entries.
    #As this one is developed for customers, less form entries are available
    #Authors:
    #	Jacob McDonald n8616469
    def customerSearchModels(self, formStore, formModel):
		#Retrieve relevant data for each field from the html page
        dailyRate = formModel['dailyRate'].value()
        make = formModel['make'].value()
        modelName = formModel['modelName'].value()
        series = formModel['series'].value()
        seatingCapacity = formModel['seatingCapacity'].value()
        standardTransmission = formModel['standardTransmission'].value()

        #Filter all vehicles based upon whether or not the meet the search critera
        vehicles = self.filter(dailyRate__icontains=dailyRate, make__icontains=make, modelName__icontains=modelName, series__icontains=series,
			seatingCapacity__icontains=seatingCapacity,standardTransmission__icontains=standardTransmission)

		#Return the remaining vehicles
        return vehicles


#This model tracks the different types of cars
#Authors:
    #   Jonathan Poulton n9731008
class Car_Model(models.Model):
    modelID = models.AutoField(primary_key=True)
    make = models.CharField(max_length=45)
    modelName = models.CharField(max_length=45)
    series = models.CharField(max_length=45)
    seriesYear = models.IntegerField()
    priceNew = models.IntegerField()
    engineSize = models.CharField(max_length=45)
    fuelSystem = models.CharField(max_length=45)
    tankCapacity = models.CharField(max_length=45)
    power = models.CharField(max_length=45)
    seatingCapacity = models.IntegerField()
    standardTransmission = models.CharField(max_length=45)
    bodyType = models.CharField(max_length=45)
    drive = models.CharField(max_length=45)
    wheelBase = models.CharField(max_length=45)
    imgSource = models.CharField(max_length=45)
    dailyRate = models.IntegerField()

    objects = Model_Manager()

    def __str__(self):
        return self.make + " " + self.modelName + " " + self.series + " " + str(self.seriesYear)

    class Meta:
        db_table = 'models'

#This class access the dataset for all cars so that it can be easily searched
#Authors:
    #   Jonathan Poulton n9731008
class Car_Manager(models.Manager):
    #This method was implemented by Jonathan Poulton n9731008
    #This method allows for cars to be searched based of the form passed in
    def SearchCars(self, searchForm, staff):
        modelIDs = []
        models = Car_Model.objects.SearchModels(searchForm)
        [modelIDs.append(model.modelID) for model in models]

        vehicles = self.filter(carStatus='available', model__in=modelIDs, store=staff.store)
        return vehicles

    def SearchCustomerCars(self, formStore, formModel):
        modelIDs = []

        models = Car_Model.objects.customerSearchModels(formStore, formModel)
        [modelIDs.append(model.modelID) for model in models]

        try:
            store_id = formStore['store_id'].value()
            store = Store.objects.get(storeID = store_id)
            vehicles = self.filter(store=store, carStatus='available', model__in=modelIDs)
            return vehicles
        except ValueError:
            vehicles = self.filter(carStatus='available', model__in=modelIDs)

        return vehicles


#This model stores information for individual cars
 #Authors:
    #   Jonathan Poulton n9731008
class Car(models.Model):
    AVAILABLE = 'available'
    RENTED = 'rented'
    statusOptions = (
        (AVAILABLE, "Available"),
        (RENTED, "Rented"),

    )
    carID = models.AutoField(primary_key=True)
    carStatus = models.CharField(max_length=45, choices=statusOptions)
    model = models.ForeignKey('Car_Model', models.DO_NOTHING)
    store = models.ForeignKey('Store', models.DO_NOTHING)

    objects = Car_Manager()


    def __str__(self):
        return str(self.carID)

    class Meta:
        db_table = 'cars'

#This Manager access the full dataset for pickups so that it can be searched
#Authors:
    #   Jonathan Poulton n9731008
class Pickups_Manager(models.Manager):
    #This method allows for the total number of rentals for a store specific store.
    #It can also restrict this to a specific time period.
    #Authors:
        #   Jonathan Poulton n9731008
    def TotalHiresForStore(self, storeID, timePeriod):
        #Check if a time period has been specified
        if not timePeriod == "":
            #Get the time period being searched
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))

            #Set the sql and parameters that will be run
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                WHERE pickupStore_id = %s AND
                pickupDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            #Set the sql and parameters that will be searched
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]

        #Execute the sql query and return the first result in the list
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    #This method finds the most popular vehicle type for a store.
    #A time period can also be specified to restrict the results.
    #Authors:
        #   Jonathan Poulton n9731008
    def MostPopularVehicleStore(self, storeID, timePeriod):
        #Check if a time period has been specified
        if not timePeriod == "":
            #Get the time period and calculate the date range to be searched
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))

            #Set the sql and parameters that will be executed
            sql = '''
                SELECT m.modelID, COUNT(p.rentalID) AS totalRentals
                FROM models AS m INNER JOIN cars AS c
                on m.modelID = c.model_id
                INNER JOIN pickups AS p
                on c.carId = p.car_id
                WHERE pickupStore_id = %s AND
                pickupDate BETWEEN %s AND %s
                GROUP BY m.modelID
                ORDER BY totalRentals DESC, m.modelID ASC
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            #Set the sql and parameters for the query
            sql = '''
                SELECT m.modelID, COUNT(p.rentalID) AS totalRentals
                FROM models AS m INNER JOIN cars AS c
                on m.modelID = c.model_id
                INNER JOIN pickups AS p
                on c.carId = p.car_id
                WHERE pickupStore_id = %s
                GROUP BY m.modelID
                ORDER BY totalRentals DESC, m.modelID ASC
                '''
            parameters = [storeID]

        #Execute the query and return the first result
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        modelID = results[0][0]
        model = str(Car_Model.objects.get(modelID=modelID))
        return model

    #This method searches for the most popular body type for a particular store
    #A time period can also be specified to limit the results
    #Authors:
        #   Jonathan Poulton n9731008
    def MostPopularBodyTypeStore(self, storeID, timePeriod):
        #Check if a time period has been specifie
        if not timePeriod == "":
            #Get the date range for the search
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))

            #Set the sql and parameters to be executed
            sql = '''
                SELECT m.bodyType, COUNT(p.rentalID) AS totalRentals
                FROM pickups AS p INNER JOIN cars AS c on
                p.car_id = c.carID INNER JOIN models AS m on
                m.modelID = c.model_id
                WHERE p.pickupStore_id = %s AND
                pickupDate BETWEEN %s AND %s
                GROUP BY m.bodyType
                ORDER BY totalRentals DESC
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            #set the sql and parameters to be executed
            sql = '''
                SELECT m.bodyType, COUNT(p.rentalID) AS totalRentals
                FROM pickups AS p INNER JOIN cars AS c on
                p.car_id = c.carID INNER JOIN models AS m on
                m.modelID = c.model_id
                WHERE p.pickupStore_id = %s
                GROUP BY m.bodyType
                ORDER BY totalRentals DESC
                '''
            parameters = [storeID]

        #Execute the query and return the first result
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    #This method gets the most popular transmission type for a store
    #A time period can also be specified
    #Authors:
        #   Jonathan Poulton n9731008
    def MostPopularTransmissionStore(self, storeID, timePeriod):
        #Check if a time period has been specified
        if not timePeriod == "":
            #Get the date range to be searched
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))

            #Set the sql query and parameters to be searched
            sql = '''
                SELECT m.standardTransmission, COUNT(p.rentalID) AS totalRentals
                FROM pickups AS p INNER JOIN cars AS c on
                p.car_id = c.carID INNER JOIN models AS m on
                m.modelID = c.model_id
                WHERE p.pickupStore_id = %s AND
                pickupDate BETWEEN %s AND %s
                GROUP BY m.standardTransmission
                ORDER BY totalRentals DESC
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            #Set the sql and parameters to be executed
            sql = '''
                SELECT m.standardTransmission, COUNT(p.rentalID) AS totalRentals
                FROM pickups AS p INNER JOIN cars AS c on
                p.car_id = c.carID INNER JOIN models AS m on
                m.modelID = c.model_id
                WHERE p.pickupStore_id = %s
                GROUP BY m.standardTransmission
                ORDER BY totalRentals DESC
                '''
            parameters = [storeID]

        #Execute the query and return the results
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        if 'M' in count:
            return 'Manual'
        return 'Automatic'

    #This method converts a sql cursor list into a dictionary
    #This method was sources from https://docs.djangoproject.com/en/2.1/topics/db/sql/
    def dictfetchall(cursor):
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    #This method returns a list with rental information for each car model for a specified branch
    #This can further be filtered using a time period and vehicle information
    #Authors:
        #   Jonathan Poulton n9731008
    def RentalPerformanceSearch(self, storeID, make, modelName, series, timePeriod):
        #Add % to the start and end of make, modelName and series so that they can be used in
        #a 'like' sql query
        make = '%' + make + '%'
        modelName = '%' + modelName + '%'
        series = '%' + series + '%'

        #Check if a time period was specified
        if not timePeriod == "":
            #Get the time period specified
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))

            #Set the sql and parameters to be executed
            sql = '''
                SELECT m.modelID, m.modelName, m.make, m.series, m.seriesYear, COUNT(DISTINCT p.rentalID) AS rentedTotal, COUNT(DISTINCT c.carID) AS numberOwned, ROUND((COUNT(DISTINCT p.rentalID)/COUNT(DISTINCT c.carID)),1) as averageRented
                FROM models AS m INNER JOIN cars AS c
                on m.modelID = c.model_id
                INNER JOIN pickups AS p
                on p.car_id = c.carID
                WHERE m.make LIKE %s AND
                m.modelName LIKE %s AND
                m.series LIKE %s AND
                p.pickupStore_id = %s AND
                pickupDate BETWEEN %s AND %s
                GROUP BY m.modelID
                ORDER BY rentedTotal DESC, m.modelID ASC
                '''
            parameters = [make, modelName, series, storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            #Set the sql and parameters to be run
            sql = '''
                SELECT m.modelID, m.modelName, m.make, m.series, m.seriesYear, COUNT(DISTINCT p.rentalID) AS rentedTotal, COUNT(DISTINCT c.carID) AS numberOwned, ROUND((COUNT(DISTINCT p.rentalID)/COUNT(DISTINCT c.carID)),1) as averageRented
                FROM models AS m INNER JOIN cars AS c
                on m.modelID = c.model_id
                INNER JOIN pickups AS p
                on p.car_id = c.carID
                WHERE m.make LIKE %s AND
                m.modelName LIKE %s AND
                m.series LIKE %s AND
                p.pickupStore_id = %s
                GROUP BY m.modelID
                ORDER BY rentedTotal DESC, m.modelID ASC
                '''
            parameters = [make, modelName, series, storeID]

        #Run the query and return the results
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = Pickups_Manager.dictfetchall(cursor)
        return results


    #This method finds all rentals that have not been returned for the assigned customer
    #This can be filtered by a specific date or store
    #Authors:
    #   Jonathan Poulton n9731008
    def ActiveRentals(self, customerID, pickupDate, pickupStoreID):
        #Get the list of closed rentals
        sql = '''
            SELECT p.rentalID
            FROM pickups AS p INNER JOIN
            returns AS r ON
            p.rentalID = r.rental_id
            WHERE p.customer_id = %s
        '''
        parameters =[customerID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            returns = cursor.fetchall()

        #This if statement checks the combination of search parameters selected
        if pickupStoreID == "" and pickupDate == "":
            #Sets the sql and parameters for the query with no filters
            sql = '''
                SELECT p.rentalID AS rentalID, p.pickupDate AS pickupDate, m.make AS make, m.modelName AS modelName,
                    m.series AS series, s.name AS pickupStore
                FROM pickups AS p INNER JOIN
                    stores AS s ON
                    s.storeID = p.pickupStore_id
                    INNER JOIN cars as c ON
                    c.carID = p.car_id
                    INNER JOIN models as m ON
                    m.modelID = c.model_id
                WHERE p.customer_id = %s AND
                    p.rentalID NOT IN %s
                GROUP BY p.rentalID
            '''
            parameters = [customerID, returns]
        elif not pickupStoreID == "" and pickupDate == "":
            #Set the sql and parameters for filtering by pickup store
            sql = '''
                SELECT p.rentalID AS rentalID, p.pickupDate AS pickupDate, m.make AS make, m.modelName AS modelName,
                    m.series AS series, s.name AS pickupStore
                FROM pickups AS p INNER JOIN
                    stores AS s ON
                    s.storeID = p.pickupStore_id
                    INNER JOIN cars as c ON
                    c.carID = p.car_id
                    INNER JOIN models as m ON
                    m.modelID = c.model_id
                WHERE p.customer_id = %s AND
                    p.rentalID NOT IN %s AND
                    p.pickupStore_id = %s
                GROUP BY p.rentalID
            '''
            parameters = [customerID, returns, pickupStoreID]
        elif pickupStoreID == "" and not pickupDate == "":
            #Set the sql and parameters for filtering by pickup date
            sql = '''
                SELECT p.rentalID AS rentalID, p.pickupDate AS pickupDate, m.make AS make, m.modelName AS modelName,
                    m.series AS series, s.name AS pickupStore
                FROM pickups AS p INNER JOIN
                    stores AS s ON
                    s.storeID = p.pickupStore_id
                    INNER JOIN cars as c ON
                    c.carID = p.car_id
                    INNER JOIN models as m ON
                    m.modelID = c.model_id
                WHERE p.customer_id = %s AND
                    p.rentalID NOT IN %s AND
                    p.pickupDate = %s
                GROUP BY p.rentalID
            '''
            parameters = [customerID, returns, pickupDate]
        elif not pickupStoreID == "" and not pickupDate == "":
            #Set the sql and parameters for filtering by both pickup store and date
            sql = '''
                SELECT p.rentalID AS rentalID, p.pickupDate AS pickupDate, m.make AS make, m.modelName AS modelName,
                    m.series AS series, s.name AS pickupStore
                FROM pickups AS p INNER JOIN
                    stores AS s ON
                    s.storeID = p.pickupStore_id
                    INNER JOIN cars as c ON
                    c.carID = p.car_id
                    INNER JOIN models as m ON
                    m.modelID = c.model_id
                WHERE p.customer_id = %s AND
                    p.rentalID NOT IN %s AND
                    p.pickupStore_id = %s AND
                    p.pickupDate = %s
                GROUP BY p.rentalID
            '''
            parameters = [customerID, returns, pickupStoreID, pickupDate]
        #Execute the sql query
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = Pickups_Manager.dictfetchall(cursor)

        return results


    #This method creates a new rental and assigned all forign key constraints
    #Authors:
    #   Jonathan Poulton n9731008
    def CreatePickup(self, customer, staff, car):
        startDate=datetime.datetime.now()
        pickupDate=datetime.datetime.now()
        car = car
        customer = customer
        pickupStore = staff.store
        staff = staff
        car.carStatus='rented'
        car.save()
        self.create(pickupStore=pickupStore, startDate=startDate, pickupDate=pickupDate,
            car=car, customer=customer, staff=staff)


#This models stores the data for pickups
#Authors:
    #   Jonathan Poulton n9731008
class Pickup(models.Model):
    rentalID = models.AutoField(primary_key=True)
    startDate = models.CharField(max_length=75)
    pickupStore = models.ForeignKey('Store', models.DO_NOTHING)
    pickupDate = models.DateField()
    car = models.ForeignKey('Car', models.DO_NOTHING)
    customer = models.ForeignKey('Customer', models.DO_NOTHING)
    staff = models.ForeignKey('Staff', models.DO_NOTHING)

    objects = Pickups_Manager()


    class Meta:
        db_table = 'Pickups'

#This manager allows for returns to be created and searched by access the dataset
#Authors:
    #   Jonathan Poulton n9731008
class Return_Manager(models.Manager):
    def dictfetchall(cursor):
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]

    #This method creates a new return
    #Authors:
    #   Jonathan Poulton n9731008
    def ReturnCar(self, staff, rental, customer):
        car = rental.car
        car.carStatus = 'available'
        car.store = staff.store
        car.save()
        Return.objects.create(rental=rental, returnStore=staff.store, returnDate=datetime.datetime.now())

    #This method finds all returns linked to a customer
    #Authors:
    #   Jonathan Poulton n9731008
    def ClosedRentals(self, customerID, pickupStoreID, pickupDate):
        if pickupStoreID == "" and pickupDate == "":
            sql = '''
                SELECT r.rental_id AS rentalID, p.pickupDate AS pickupDate, p.pickupStore_id AS pickupStore, r.returnStore_id AS returnStore, p.car_id AS car, r.returnDate AS returnDate, p.startDate AS startDate
                FROM pickups as p INNER JOIN
                    returns as r ON
                    r.rental_id = p.rentalID INNER JOIN
                    stores AS s ON
                    p.pickupStore_id = s.storeID
                WHERE p.customer_id = %s
                GROUP BY r.rental_id
            '''
            parameters = [customerID]
        elif not pickupStoreID == "" and pickupDate == "":
            sql = '''
                SELECT r.rental_id AS rentalID, p.pickupDate AS pickupDate, p.pickupStore_id AS pickupStore, r.returnStore_id AS returnStore, p.car_id AS car, r.returnDate AS returnDate, p.startDate AS startDate
                FROM pickups as p INNER JOIN
                    returns as r ON
                    r.rental_id = p.rentalID INNER JOIN
                    stores AS s ON
                    p.pickupStore_id = s.storeID
                WHERE p.customer_id = %s AND
                    s.storeID = %s
                GROUP BY r.rental_id
            '''
            parameters = [customerID, pickupStoreID]
        elif pickupStoreID == "" and not pickupDate == "":
            sql = '''
                SELECT r.rental_id AS rentalID, p.pickupDate AS pickupDate, p.pickupStore_id AS pickupStore, r.returnStore_id AS returnStore, p.car_id AS car, r.returnDate AS returnDate, p.startDate AS startDate
                FROM pickups as p INNER JOIN
                    returns as r ON
                    r.rental_id = p.rentalID INNER JOIN
                    stores AS s ON
                    p.pickupStore_id = s.storeID
                WHERE p.customer_id = %s AND
                    p.pickupDate = %s
                GROUP BY r.rental_id
            '''
            parameters = [customerID, pickupDate]
        elif not pickupStoreID == "" and not pickupDate == "":
            sql = '''
                SELECT r.rental_id AS rentalID, p.pickupDate AS pickupDate, p.pickupStore_id AS pickupStore, r.returnStore_id AS returnStore, p.car_id AS car, r.returnDate AS returnDate, p.startDate AS startDate
                FROM pickups as p INNER JOIN
                    returns as r ON
                    r.rental_id = p.rentalID INNER JOIN
                    stores AS s ON
                    p.pickupStore_id = s.storeID
                WHERE p.customer_id = %s AND
                    p.pickupDate = %s AND
                    s.storeID = %s
                GROUP BY r.rental_id
            '''
            parameters = [customerID, pickupDate, pickupStoreID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = Return_Manager.dictfetchall(cursor)
        return results

#This model stores information for rentals that have been returned
#Authors:
    #   Jonathan Poulton n9731008
class Return(models.Model):
    rental = models.OneToOneField('Pickup',  models.DO_NOTHING, primary_key=True)
    returnStore = models.ForeignKey('Store', models.DO_NOTHING)
    returnDate = models.DateField()

    objects = Return_Manager()


    class Meta:
        db_table = 'Returns'

#This model stores all of the SQL queries used for the store data reporting
#Authors:
    #   Jacob McDonald n8616469

class Store_Manager(models.Manager):
    stores = 0
    stateStores = 0

    def TotalStores(self, storeID, timePeriod):
        if not timePeriod == "":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(Distinct storeID)
                FROM stores
                '''
            parameters = []
        else:
            sql = '''
                SELECT COUNT(Distinct storeID)
                FROM stores
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        global stores
        stores = count
        return count

    def TotalStateStores(self, storeID, timePeriod):
        if not timePeriod == "":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(Distinct storeID)
                FROM stores
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        else:
            sql = '''
                SELECT COUNT(Distinct storeID)
                FROM stores
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        global stateStores
        stateStores = count
        return count

    def TotalVehicles(self, storeID, timePeriod):
        if not timePeriod == "Total":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                WHERE store_id = %s
                '''
            parameters = [storeID]
        else:
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                WHERE store_id = %s
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalNationalVehicles(self, storeID, timePeriod):
        if not timePeriod == "Total":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                '''
            parameters = []
        else:
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def AverageNationalVehicles(self, storeID, timePeriod):
        if not timePeriod == "Total":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                '''
            parameters = []
        else:
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return round(count/stores)

    def TotalStateVehicles(self, storeID, timePeriod):
        if not timePeriod == "Total":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                Join stores ON stores.storeID = cars.store_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        else:
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                Join stores ON stores.storeID = cars.store_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def AverageStateVehicles(self, storeID, timePeriod):
        if not timePeriod == "Total":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                Join stores ON stores.storeID = cars.store_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        else:
            sql = '''
                SELECT COUNT(carID) AS total
                FROM cars
                Join stores ON stores.storeID = cars.store_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return round(count/stateStores)

    def TotalCustomers(self, storeID, timePeriod):
        if not timePeriod == "Total":
            #today = datetime.datetime.today()
            #startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
        else:
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
            #parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalActiveCustomers(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s AND
                pickupDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalActiveStateCustomers(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(DISTINCT customer_id) AS totalCustomers
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s) AND
                pickupDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(DISTINCT customer_id) AS totalCustomers
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def AverageStateCustomers(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(DISTINCT customer_id) AS totalCustomers
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s) AND
                pickupDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(DISTINCT customer_id) AS totalCustomers
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return round(count/stateStores)

    def Profit(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns, pickups, cars, models
                WHERE returns.rental_id = pickups.rentalID AND
                pickups.car_id = cars.carID AND
                cars.model_id = models.modelID AND
                returnStore_id = %s AND
                returnDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns, pickups, cars, models
                WHERE returns.rental_id = pickups.rentalID AND
                pickups.car_id = cars.carID AND
                cars.model_id = models.modelID AND
                returnStore_id = %s
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]

        if count is None:
            return '$0.00'

        return '${:,.2f}'.format(count)


    def TotalNationalProfit(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns, pickups, cars, models
                WHERE returns.rental_id = pickups.rentalID AND
                pickups.car_id = cars.carID AND
                cars.model_id = models.modelID AND
                returnDate BETWEEN %s AND %s
                '''
            parameters = [startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns, pickups, cars, models
                WHERE returns.rental_id = pickups.rentalID AND
                pickups.car_id = cars.carID AND
                cars.model_id = models.modelID
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]

        if count is None:
            return '$0.00'

        return '${:,.2f}'.format(count)


    def AverageNationalProfit(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns, pickups, cars, models
                WHERE returns.rental_id = pickups.rentalID AND
                pickups.car_id = cars.carID AND
                cars.model_id = models.modelID AND
                returnDate BETWEEN %s AND %s
                '''
            parameters = [startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns, pickups, cars, models
                WHERE returns.rental_id = pickups.rentalID AND
                pickups.car_id = cars.carID AND
                cars.model_id = models.modelID
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        if count is None:
            return '$0.00'

        return '${:,.2f}'.format(count/stores)

    def TotalStateProfit(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns
                JOIN pickups ON returns.rental_id = pickups.rentalID
                JOIN cars ON pickups.car_id = cars.carID
                JOIN models ON cars.model_id = models.modelID
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s) AND
                returnDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns
                JOIN pickups ON returns.rental_id = pickups.rentalID
                JOIN cars ON pickups.car_id = cars.carID
                JOIN models ON cars.model_id = models.modelID
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]

        if count is None:
            return '$0.00'

        return '${:,.2f}'.format(count)

    def AverageStateProfit(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns
                JOIN pickups ON returns.rental_id = pickups.rentalID
                JOIN cars ON pickups.car_id = cars.carID
                JOIN models ON cars.model_id = models.modelID
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s) AND
                returnDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                Select SUM(DATEDIFF(returnDate,pickupDate) * dailyRate)
                FROM returns
                JOIN pickups ON returns.rental_id = pickups.rentalID
                JOIN cars ON pickups.car_id = cars.carID
                JOIN models ON cars.model_id = models.modelID
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]

        if count is None:
            return '$0.00'

        return '${:,.2f}'.format(count/stateStores)

    def AverageNationalCustomers(self, store, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                WHERE pickupDate BETWEEN %s AND %s
                '''
            parameters = [startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return round(count/stores)

    def TotalActiveNationalCustomers(self, store, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                WHERE pickupDate BETWEEN %s AND %s
                '''
            parameters = [startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(DISTINCT p.customer_ID) AS totalCustomers
                FROM pickups AS p
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalRentalsForStore(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                WHERE pickupStore_id = %s AND
                pickupDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalReturnedRentals(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(rental_id) AS total
                FROM returns
                WHERE returnStore_id = %s AND
                returnDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(rental_id) AS total
                FROM returns
                WHERE returnStore_id = %s
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalNationalRentals(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                WHERE pickupDate BETWEEN %s AND %s
                '''
            parameters = [startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalAverageNationalRentals(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                WHERE pickupDate BETWEEN %s AND %s
                '''
            parameters = [startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                '''
            parameters = []
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return round(count/stores)

    def TotalStateRentals(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s) AND
                pickupDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def TotalAverageStateRentals(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s) AND
                pickupDate BETWEEN %s AND %s
                '''
            parameters = [storeID, startDate.strftime('%Y-%m-%d'), today.strftime('%Y-%m-%d')]
        else:
            sql = '''
                SELECT COUNT(rentalID) AS total
                FROM pickups
                Join stores ON stores.storeID = pickups.pickupStore_id
                WHERE stores.State = (SELECT state FROM stores WHERE storeID = %s)
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return round(count/stateStores)

    def FirstRental(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT MIN(pickupDate) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
        else:
            sql = '''
                SELECT MIN(pickupDate) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count

    def RecentRentals(self, storeID, timePeriod):
        if not timePeriod == "":
            today = datetime.datetime.today()
            startDate = today - datetime.timedelta(days=int(timePeriod))
            sql = '''
                SELECT MAX(pickupDate) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
        else:
            sql = '''
                SELECT MAX(pickupDate) AS totalCustomers
                FROM pickups AS p
                WHERE pickupStore_id = %s
                '''
            parameters = [storeID]
        with connection.cursor() as cursor:
            cursor.execute(sql, parameters)
            results = cursor.fetchall()
        count = results[0][0]
        return count




#This model stores information for the business's stores
#Authors:
    #   Jonathan Poulton n9731008
class Store(models.Model):
    storeID = models.AutoField(primary_key=True)
    name = models.CharField(max_length=75)
    address = models.CharField(max_length=75)
    city = models.CharField(max_length=75)
    state = models.CharField(max_length=15)
    phone = models.CharField(max_length=30, blank=True, null=True)

    objects = Store_Manager()


    def __str__(self):
        return self.name

    class Meta:
        db_table = 'stores'

#The inspection manager is used to submit an inspection report
#Created by Mattew Keye
class Inspection_Manager(models.Manager):
    def CreateInspection(self, comments, mechanic, dateCompleted, rentalID):
        comments = comments
        mechanic = mechanic
        dateCompleted = dateCompleted

        formattedDate = datetime.datetime.strptime(dateCompleted,'%m/%d/%Y').strftime('%Y-%m-%d')

        rentalID = rentalID

        self.create(comments=comments, mechanic=mechanic, dateCompleted=formattedDate, rental_id=rentalID)




#This model stores information for inspections
#Authors:
    #   Jonathan Poulton n9731008
class Inspection(models.Model):
    inspectionID = models.AutoField(primary_key=True)
    comments = models.CharField(max_length=255)
    dateCompleted = models.DateField()
    mechanic = models.ForeignKey('Staff', models.DO_NOTHING)
    rental = models.ForeignKey('Return', models.DO_NOTHING)

    objects = Inspection_Manager()

    class Meta:
        db_table = 'inspections'

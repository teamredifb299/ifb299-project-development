from django.contrib import admin

# Register your models here.
from .models import Account, Customer, Staff, Store, Car, Car_Model, Pickup, Return, Inspection, Role
admin.site.register(Account)
admin.site.register(Role)
admin.site.register(Customer)
admin.site.register(Staff)
admin.site.register(Store)
admin.site.register(Car)
admin.site.register(Car_Model)
admin.site.register(Pickup)
admin.site.register(Return)
admin.site.register(Inspection)

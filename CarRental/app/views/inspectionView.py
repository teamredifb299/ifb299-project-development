from django.shortcuts import render, redirect
from django.http import HttpResponse
from app.models import Account, User_Authentication, Car, Car_Model, Inspection, Pickup, Customer, Staff
from app.forms import inspectionForms

#View the inpsection report for a customerID
#Authors:
    #Matthew Keye   n9697403
def ViewCustomerInspectionReport(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    #Check if the user is logged in and has staff access
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    #Check how the page was accessed
    if request.method == 'GET':
        try:
            rentalID = request.GET['rentalId']
            customerName = request.GET['customerId']
        except Exception as e:
            return redirect('/CarRental/home')

        rentals = Pickup.objects.get(rentalID=rentalID)
        context_dict['rentals'] = rentals

        customerID = Pickup.objects.filter(rentalID=rentalID).values_list('customer_id', flat=True)

        foundCustomerID = customerID[0]
        customer = Customer.objects.get(userID=foundCustomerID)
        context_dict['customer'] = customerID


        try:
            inspectionReport = Inspection.objects.get(rental_id=rentalID)
            context_dict['inspectionReport'] = inspectionReport
        except Exception as e:
            context_dict['noInspection'] = True
            return render(request, 'Inspection/viewInspectionReport.html', context_dict)

        return render(request, 'Inspection/viewInspectionReport.html', context_dict)
    else:
        return redirect('/CarRental/home')

def inspection_added(request):
    return render(request, 'Inspection/inspection_submitted.html')

#Display the inspection submission page and submit to database if input passes validation
#Authors:
    #Matthew Keye   n9697403
def submit_inspection(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:

        mechanic_access = User_Authentication.MECHANIC_ROLE
        if not User_Authentication.AccessLevelMet(request, mechanic_access):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsMechanic'] = True
    else:
        return redirect('/CarRental/login')

    search_form = inspectionForms.submit_inspection()

    if request.method == 'POST':
        inspectionSubmissionForm = inspectionForms.create_inspection(request.POST)

        #data = request.POST.copy()
        if inspectionSubmissionForm.is_valid():

            inspection_form = request.POST

            comments = inspection_form['comments']
            mechanic = Staff.objects.get(userID=request.session['user'])
            dateCompleted = inspection_form['dateCompleted']
            rentalID = inspection_form['rentalID']
            Inspection.objects.CreateInspection(comments, mechanic, dateCompleted, rentalID)
            #test333 = inspection_tools().submitInspection(request)
            #inspect_add = inspectionSubmissionForm.save()
            return redirect('/CarRental/CustomerTools/inspectionReport/submitted')
        else:
            inspection_form = request.POST
            url = '/CarRental/CustomerTools/inspectionReport?carID=' + inspection_form['carID']
            return redirect(url)
        #return render(request, url, context_dict)
    else:

        if 'carID' in request.GET and request.GET['carID']:

            carID = request.GET['carID']
            rentalID = Pickup.objects.filter(car_id=carID).values_list('rentalID', flat=True).order_by('rentalID')


            carList = Car.objects.filter(carID=carID, carStatus="Available")

            numberFound = 0

            for number in carList:
                numberFound = numberFound + 1

            if numberFound == 1:
                context_dict['carList'] = carList

                try:
                    foundID = rentalID[0]
                except Exception as e:
                    context_dict['noData'] = True
                    context_dict['notFirstLoad'] = True
                    context_dict['searchForm'] = search_form

                    return render(request, 'Inspection/inspection_search.html', context_dict)



                carModel = carList.values('model_id')

                carInfo = {}
                carInfo = Car_Model.objects.filter(modelID=carModel)
                context_dict['modelList'] = carInfo

                context_dict['rentalID'] = foundID

                submit_form = inspectionForms.create_inspection()
                context_dict['inspectionForm'] = submit_form

            else:
                context_dict['noData'] = True


            context_dict['notFirstLoad'] = True
            context_dict['searchForm'] = search_form

            return render(request, 'Inspection/inspection_search.html', context_dict)
        else:
            context_dict['searchForm'] = search_form
            return render(request, 'Inspection/inspection_search.html', context_dict)

from app.models import Account, Customer, Car, Return, Store, Staff, Car_Model, Pickup, User_Authentication
from django.shortcuts import render, render_to_response, redirect
from app.forms import customerSearchForms

def Home(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    stores = Store.objects.all()
    context_dict['stores'] = stores
    #Check if the user is logged in and has staff access
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        mechanicAccess = User_Authentication.MECHANIC_ROLE

        if User_Authentication.AccessLevelMet(request, mechanicAccess):
            context_dict['userIsMechanic'] = True
        elif User_Authentication.AccessLevelMet(request, accessRequired):
            context_dict['userIsStaff'] = True

    #Get the customer  and form information and send them to the dictionary
    context_dict['customerSearchForm'] = customerSearchForms.Customer_Search_Car_Store_Form() 
    context_dict['customerSearchForm1'] = customerSearchForms.Customer_Search_Car_Form()
    
    #If a search was completed then find the results
    if request.method == "POST":
        formStore = customerSearchForms.Customer_Search_Car_Store_Form(request.POST)
        formModel = customerSearchForms.Customer_Search_Car_Form(request.POST)
        cars = Car.objects.SearchCustomerCars(formStore, formModel)
        context_dict['cars'] = cars
    return render(request, 'CustomerTools/customerCarSearch.html', context_dict)

def About(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    #Check if the user is logged in and has staff access
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        context_dict['userIsStaff'] = User_Authentication.AccessLevelMet(request, accessRequired)
    #Get the customer  and form information and send them to the dictionary
    return render(request, 'aboutUs.html', context_dict)

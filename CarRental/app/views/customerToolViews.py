from app.models import Account, Customer, Car, Return, Store, Staff, Car_Model, Pickup, User_Authentication
from django.shortcuts import render, render_to_response, redirect
import datetime
from app.forms import accountManagementForms as AccountForms
from app.forms import customerToolForms as ToolForms

def ViewRental(request):
    # assuming dict used as temporary storage solution within browser
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    #Check if the user is logged in and has staff access
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        loggedInStatus = True
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    rentalID = request.GET['id']
    print(rentalID)

    #Get the customer and send it to the html
    pickup = Pickup.objects.get(rentalID = rentalID)
    # Return = Return.objects.get(rentalID = rentalID)
    context_dict['pickup'] = pickup
    # context_dict['return1'] = return1/
    # rental = Pickup.objects.get(rentalID=request.GET['rentalID'])/
    context_dict['customer'] = pickup.customer
    context_dict['car'] = pickup.car

    #Get the staff member and check their access, then add them to the dictionary
    staff = Staff.objects.get(userID = request.session['user'])
    context_dict['user'] = staff.staffName
    context_dict['manager'] = staff.AccessRequiredManager()
    try:
        returnInfo = Return.objects.get(rental=pickup)
        context_dict['return'] = returnInfo
    except Exception as e:
        returnInfo = ""

    return render(request, 'CustomerTools/customerRentalStatus.html', context_dict)

def StoreInformation(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    #Check if the user is logged in and has staff access
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        loggedInStatus = True
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    storeID = request.GET['storeID']
    store = Store.objects.get(storeID = storeID)

    context_dict['store'] = store

    return render(request, 'CustomerTools/StoreInfoPage.html', context_dict)

#Search for a customer as a staff member
    #Authors:
        #Matthew Keye    n9697403
def Search(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE

        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    search_form = ToolForms.Search_Customer_Info()

    if 'name' in request.GET and request.GET['name'] or 'phone' in request.GET and request.GET['phone'] or 'license' in request.GET and request.GET['license']:
        name = request.GET['name']
        phone = request.GET['phone']
        license = request.GET['license']

        nameList = Customer.objects.filter(name__icontains=name)
        phoneList = Customer.objects.filter(phone__icontains=phone)
        licenseList = Customer.objects.filter(license__icontains=license)

        nameSelected = False
        phoneSelected = False
        licenseSelected = False

        if name != '':
            nameSelected = True

        if phone != '':
            phoneSelected = True

        if license != '':
            licenseSelected = True

        #if only name is entered
        if nameSelected and not phoneSelected and not licenseSelected:
            customerList = Customer.objects.filter(name__icontains=name)
        #if only phone is entered
        if not nameSelected and phoneSelected and not licenseSelected:
            customerList = Customer.objects.filter(phone__icontains=phone)
        #if only license is entered
        if not nameSelected and not phoneSelected and licenseSelected:
            customerList = Customer.objects.filter(license__icontains=license)
        #if name and phone is entered
        if nameSelected and phoneSelected and not licenseSelected:
            customerList = Customer.objects.filter(name__icontains=name, phone__icontains=phone)
        #if name and license is entered
        if nameSelected and not phoneSelected and licenseSelected:
            customerList = Customer.objects.filter(name__icontains=name, license__icontains=license)
        #if phone and license is entered
        if not nameSelected and phoneSelected and licenseSelected:
            customerList = Customer.objects.filter(phone__icontains=phone, license__icontains=license)
        #if name, phone and license is entered
        if nameSelected and phoneSelected and licenseSelected:
            customerList = Customer.objects.filter(name__icontains=name, phone__icontains=phone, license__icontains=license)

        context_dict['customerListData'] = customerList
        if not customerList:
            context_dict['noData'] = True
        context_dict['notFirstLoad'] = True
        context_dict['searchForm'] = search_form

        return render(request, 'CustomerTools/customer-search.html', context_dict)
    else:
        context_dict['searchForm'] = search_form
        return render(request, 'CustomerTools/customer-search.html', context_dict)

def CreateCustomer(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    if request.method == 'POST':
        registerForm = AccountForms.Register_Form(request.POST)

        if registerForm.is_valid():
            register = registerForm.save()
            return redirect('/CarRental/CustomerTools/customerAdded')
    else:
        details_form = AccountForms.Register_Form()

    context_dict['register_form'] = details_form

    return render(request, 'customerTools/create-customer.html', context_dict)

def CustomerAdded(request):
    return render(request, 'customerTools/customer_added.html')


#This view allows for a customers information to be updated by a staff member
#Authors:
    #   Jonathan Poulton n9731008
def UpdateCustomerInfo(request):
    context_dict = {}
    updateForm = ToolForms.Update_Customer_Info()
    context_dict['updateForm'] = updateForm

    #Check if the user is logged in and has staff access
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    if request.method=="POST":
        cusForm=request.POST
        customer = Customer.objects.get(userID=cusForm['userID'])
        customer.name = cusForm['name']
        # customer.phone = cus_form['phone']
        customer.address = cusForm['address']
        customer.email = cusForm['email']
        customer.occupation = cusForm['occupation']
        customer.gender = cusForm['gender']
        customer.license = cusForm['license']
        customer.save()
        return redirect('/CarRental/CustomerTools/customer?id='+str(customer.userID))
    elif request.method=="GET":
        customer = Customer.objects.get(userID=request.GET['id'])
        context_dict['customer'] = customer
    else:
        return redirect('/CarRental/CustomerTools/search/')
    return render(request, 'CustomerTools/updateCustomerInfo.html', context_dict)

#This view allows for staff to search for a car and rnet it to a customer
#Authors:
    #   Jonathan Poulton n9731008
def SearchCar(request, customerID):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    #Check if the user is logged in and has staff access
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        loggedInStatus = True
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    #Get the customer  and form information and send them to the dictionary
    customer = Customer.objects.get(userID=customerID)
    context_dict['customer'] = customer
    context_dict['searchForm'] = ToolForms.Search_Car_Form()

    #If a search was completed then find the results
    if request.method == "GET":
        staff = Staff.objects.get(userID=request.session['user'])
        form = ToolForms.Search_Car_Form(request.GET)
        cars = Car.objects.SearchCars(form, staff)

        context_dict['cars'] = cars
    return render(request, 'CustomerTools/CarSearch.html', context_dict)


#This view creates a new rental for a customer
#Authors:
    #   Jonathan Poulton n9731008
def CreateRental(request, customer_id):
    loggedInStatus = User_Authentication.LoginStatus(request)

    #Check if the user is logged in and has staff access
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        accessMet = User_Authentication.AccessLevelMet(request, accessRequired)
        if not accessMet:
            return redirect('/CarRental/home')
    else:
        return redirect('/CarRental/login')

    #get the details for the rental and send them to the Pickup constructor
    customer = Customer.objects.get(userID=customer_id)
    staff = Staff.objects.get(userID=request.session['user'])
    car = Car.objects.get(carID=request.GET['carID'])
    Pickup.objects.CreatePickup(customer, staff, car)
    return redirect('/CarRental/CustomerTools/customer?id='+str(customer.userID))


    ##This view shows the Staff Portal
#Authors:
    #   Jacob McDonald n8616469
def staffPortal(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    #Check if the user is logged in and has staff access
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        mechanicAccess = User_Authentication.MECHANIC_ROLE
        managerAccess = User_Authentication.MANAGER_ROLE

        accessMet = User_Authentication.AccessLevelMet(request, accessRequired)
        mechanicAccessMet = User_Authentication.AccessLevelMet(request, mechanicAccess)
        managerAccessMet = User_Authentication.AccessLevelMet(request, managerAccess)

        context_dict['userIsManager'] = managerAccessMet
        if accessMet:
            context_dict['userIsStaff'] = True
        elif mechanicAccessMet:
            context_dict['userIsMechanic'] = True
        else:
            return redirect('/CarRental/login')

    else:
        return redirect('/CarRental/login')
    return render(request, 'CustomerTools/staffPortal.html', context_dict)

def CreateModel(request):
    context_dict = {}
    context_dict['stores'] = Store.objects.all().order_by('name')
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    if request.method == 'POST':
        registerForm = AccountForms.Model_Register_Form(request.POST)

        if registerForm.is_valid():
            register = registerForm.save()
            return redirect('customerTools/model_added')
    else:
        details_form = AccountForms.Model_Register_Form()

    context_dict['register_form'] = details_form

    return render(request, 'customerTools/create-model.html', context_dict)

def ModelAdded(request):
    return render(request, 'customerTools/model_added.html')

def CreateCar(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    if request.method == 'POST':
        registerForm = AccountForms.Car_Register_Form(request.POST)

        if registerForm.is_valid():
            register = registerForm.save()
            return redirect('customerTools/model_added')
    else:
        details_form = AccountForms.Car_Register_Form()

    context_dict['register_form'] = details_form

    return render(request, 'customerTools/create-car.html', context_dict)

def ModelAdded(request):
    return render(request, 'customerTools/model_added.html')

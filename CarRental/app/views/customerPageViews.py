from app.models import Account, Customer, Car, Return, Store, Staff, Car_Model, Pickup, User_Authentication
from django.shortcuts import render, redirect

#This view displays a cusotmer information including their rental history
#Authors:
    #   Jonathan Poulton n9731008
def CustomerPage(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    #Check if the user is logged in and has staff access
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    #Check how the page was accessed
    if request.method == 'GET':
        #Get the customer
        try:
            customerID = request.GET['id']
        except Exception as e:
            return redirect('/CarRental/CustomerTools/search')

        #Get the customer and send it to the html
        customer = Customer.objects.get(userID=customerID)
        context_dict['customer'] = customer

        # Get the staff member and check their access, then add them to the dictionary
        staff = Staff.objects.get(userID = request.session['user'])
        context_dict['user'] = staff.staffName
        context_dict['manager'] = staff.AccessRequiredManager()

        stores = Store.objects.all()
        context_dict['stores'] = stores
        return render(request, 'CustomerPage/customerPage.html', context_dict)
    else:
        return redirect('/CarRental/CustomerTools/search')

#This view filters the acitve rentals table
#Authors:
    #   Jonathan Poulton n9731008
def activeRentalsFilter(request):
    context_dict = {}
    #Get the parameters sent by the ajax request
    customerID = request.GET['customerID']
    pickupDate = request.GET['pickupDate']
    pickupStoreID = request.GET['pickupStoreID']
    #Filter the results and add it to the context dict
    activeRentals = Pickup.objects.ActiveRentals(customerID, pickupDate, pickupStoreID)
    context_dict['activeRentals'] = activeRentals
    return render (request, 'CustomerPage/activeRentals.html', context_dict)

#This view filters the closed rental table
#Authors:
    #   Jonathan Poulton n9731008
def closedRentalsFilter(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    #Check if the user is logged in
    if(loggedInStatus):
        accessRequired = User_Authentication.MANAGER_ROLE
        accessMet = User_Authentication.AccessLevelMet(request, accessRequired)
        #Check if the user is a staff member
        if accessMet:
            context_dict['isManager'] = True
            #Get the search parameters sent by the ajax query
            customerID = request.GET['customerID']
            pickupDate = request.GET['pickupDate']
            pickupStoreID = request.GET['pickupStoreID']

            #Get the closed renatls and send them to the dictionary
            closedRentals = Return.objects.ClosedRentals(customerID, pickupStoreID, pickupDate)
            context_dict['closedRentals'] = closedRentals      
    return render(request, 'CustomerPage/closedRentals.html', context_dict)

#This view returns a rental
#Authors:
    #   Jonathan Poulton n9731008
def ReturnCar(request):
    loggedInStatus = User_Authentication.LoginStatus(request)
    #Check if the user is logged in
    if(loggedInStatus):
        accessRequired = User_Authentication.STAFF_ROLE
        accessMet = User_Authentication.AccessLevelMet(request, accessRequired)
        #Check if the user is a staff member
        if accessMet:
            #Get the rental information form the request and send them to the constructor
            userID = request.session['user']
            staff = Staff.objects.get(userID=userID)
            rental = Pickup.objects.get(rentalID=request.GET['rentalID'])
            customer = rental.customer
            Return.objects.ReturnCar(staff, rental, customer)
            return redirect('/CarRental/CustomerTools/customer?id='+str(customer.userID))
        else:
            return redirect('/CarRental/CustomerTools/search/')
    else:
        redirect('CarRental/login')
from django.shortcuts import render, redirect
from django.http import HttpResponse
from app.models import Account, User_Authentication
from app.forms import accountManagementForms as AccountForms


def Index(request):
    return HttpResponse(request.session['user'])

#This view allows a user to login with their credentials.
#If the login is successfull they are reiderected to the home page
#Authors:
    #   Jonathan Poulton n9731008
def Login(request):
    context_dict = {}
    loggedInStatus= User_Authentication.LoginStatus(request)

    #Check if the customer is already logged in and if so redirect them
    if loggedInStatus:
        return redirect('/CarRental/home')
    else:
        #Check if post data has been sent, if not simply display the login form
        if request.method == 'POST':
            #Pull the data from the form
            loginForm = AccountForms.Login_Form()
            username = request.POST['username']
            password = request.POST['password']

            #Check if the user's data is correct and that they can be logged in
            user = User_Authentication.Login(username, password)
            if not user == False:
                #Update session variables to track the login status
                request.session['loggedIn'] = True
                request.session['user'] = user.userID

                #redirect to the home page
                return redirect('/CarRental/home')
            else:
                #Display the errors from the form
                logInError = 'Invalid username or password'
                context_dict['logInError'] = logInError
                loginFailed = True
                context_dict['logInFailed'] = loginFailed
            context_dict['loginForm'] = loginForm
        else:
            loginForm = AccountForms.Login_Form()
            context_dict['loginForm'] = loginForm

        return render(request, 'AccountManagement/loginForm.html',context_dict)

#This view logs a user out and redirects them to the login page
#Authors:
    #   Jonathan Poulton n9731008
def Logout(request):
    request.session['loggedIn'] = False
    return redirect('/CarRental/login/')

#This view allows a customer to register their account.
#Authors:
    #   Matthew Keye    n9697403
def Register(request):
    loggedInStatus= User_Authentication.LoginStatus(request)
    if loggedInStatus:
        return Index(request)
    detailsForm = AccountForms.Register_Form()
    if request.method == 'POST':
        registerForm = AccountForms.Register_Form(request.POST)

        if registerForm.is_valid():
            register = registerForm.save()
            return redirect('/CarRental/register_success')
    else:
        detailsForm = AccountForms.Register_Form()

    context_dict = {}
    context_dict['loggedIn'] = loggedInStatus
    context_dict['registerForm'] = detailsForm

    return render(request, 'AccountManagement/register.html', context_dict)

def RegisterSuccess(request):
    return render(request, 'AccountManagement/register_success.html')

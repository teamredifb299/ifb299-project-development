from app.models import Store, Car_Model, Car, Pickup, User_Authentication
from django.shortcuts import render, redirect

def CustomerPerformancePage(request):
    context_dict = {}
    context_dict['stores'] = Store.objects.all().order_by('name')
    return render(request, 'CustomerDataTool/main.html', context_dict)

def CustomerSummaryTable(request):
    context_dict = {}
    store = request.GET['storeID']
    timePeriod = request.GET['timePeriod']
    context_dict['totalStores'] = Store.objects.TotalStores(store,timePeriod)
    context_dict['totalStateStores'] = Store.objects.TotalStateStores(store,timePeriod)
    context_dict['totalVehicles'] = Store.objects.TotalVehicles(store,timePeriod)
    context_dict['totalCustomers'] = Store.objects.TotalCustomers(store,timePeriod)
    context_dict['totalActiveCustomers'] = Store.objects.TotalActiveCustomers(store,timePeriod)
    context_dict['totalRentals'] = Store.objects.TotalRentalsForStore(store, timePeriod)
    context_dict['totalNationalRentals'] = Store.objects.TotalNationalRentals(store, timePeriod)
    context_dict['totalStateRentals'] = Store.objects.TotalStateRentals(store, timePeriod)
    context_dict['totalAverageNationalRentals'] = Store.objects.TotalAverageNationalRentals(store, timePeriod)
    context_dict['totalAverageStateRentals'] = Store.objects.TotalAverageStateRentals(store, timePeriod)
    context_dict['firstRental'] = Store.objects.FirstRental(store, timePeriod)
    context_dict['recentRental'] = Store.objects.RecentRentals(store, timePeriod)
    context_dict['averageNationalCustomers'] = Store.objects.AverageNationalCustomers(store, timePeriod)
    context_dict['averageStateCustomers'] = Store.objects.AverageStateCustomers(store, timePeriod)
    context_dict['totalActiveNationalCustomers'] = Store.objects.TotalActiveNationalCustomers(store, timePeriod)
    context_dict['totalActiveStateCustomers'] = Store.objects.TotalActiveStateCustomers(store, timePeriod)
    context_dict['totalStateVehicles'] = Store.objects.TotalStateVehicles(store,timePeriod)
    context_dict['totalNationalVehicles'] = Store.objects.TotalNationalVehicles(store,timePeriod)
    context_dict['profit'] = Store.objects.Profit(store,timePeriod)
    context_dict['averageNationalVehicles'] = Store.objects.AverageNationalVehicles(store,timePeriod)
    context_dict['averageStateVehicles'] = Store.objects.AverageStateVehicles(store,timePeriod)
    context_dict['totalReturnedRentals'] = Store.objects.TotalReturnedRentals(store, timePeriod)
    context_dict['totalNationalProfit'] = Store.objects.TotalNationalProfit(store,timePeriod)
    context_dict['totalStateProfit'] = Store.objects.TotalStateProfit(store,timePeriod)
    context_dict['averageNationalProfit'] = Store.objects.AverageNationalProfit(store,timePeriod)
    context_dict['averageStateProfit'] = Store.objects.AverageStateProfit(store,timePeriod)
    return render(request, 'CustomerDataTool/customerSummaryTable.html', context_dict)

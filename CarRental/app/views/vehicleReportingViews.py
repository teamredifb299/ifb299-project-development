from app.models import Store, Car_Model, Car, Pickup, User_Authentication
from django.shortcuts import render, redirect

#Set the initial search filters for the page
#Authors:
    #   Jonathan Poulton n9731008
def VehiclePerformancePage(request):
    context_dict = {}
        
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    #Check if the user is logged in and has staff access
    if loggedInStatus:
        accessRequired = User_Authentication.MANAGER_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    context_dict['stores'] = Store.objects.all().order_by('name')
    context_dict['makes'] = Car_Model.objects.order_by('make').values('make').distinct()
    return render(request, 'BranchPerformanceTool/main.html', context_dict)

#This method displays the reports store summary table
#Authors:
    #   Jonathan Poulton n9731008
def SummaryTable(request):
    context_dict = {}
    #Get the parameters passed
    store = request.GET['storeID']
    timePeriod = request.GET['timePeriod']
    #Find the total renalts
    totalRentals = Pickup.objects.TotalHiresForStore(store, timePeriod)
    context_dict['totalHires'] = totalRentals  
    #Set the popular information to N/A as there can not be a popular option  
    if totalRentals == 0:
        context_dict['popularModel'] = "N/A"
        context_dict['popularBodytype'] = "N/A"
        context_dict['popularTransmission'] = 'N/A' 
    else:
        #Get each popular result and set them to the context dictionary
        context_dict['popularModel'] = Pickup.objects.MostPopularVehicleStore(store, timePeriod)
        context_dict['popularBodytype'] = Pickup.objects.MostPopularBodyTypeStore(store, timePeriod)
        context_dict['popularTransmission'] = Pickup.objects.MostPopularTransmissionStore(store, timePeriod)
    return render(request, 'BranchPerformanceTool/summaryTable.html', context_dict)

#This method displays the summary of the stores rental history
#Authors:
    #   Jonathan Poulton n9731008
def Search(request):
    context_dict = {}
    #Get the search parameters being search by
    storeID = request.GET['storeID']
    make = request.GET['make']
    modelName = request.GET['modelName']
    series = request.GET['series']
    timePeriod = request.GET['timePeriod']
    #Search for the table informationb
    results = Pickup.objects.RentalPerformanceSearch(storeID, make, modelName, series, timePeriod)
    context_dict['results'] = results
    return render(request, 'BranchPerformanceTool/search.html', context_dict)

#The method updates the dropdown list filter for the model name
#This is based on what is selected for the make dropdown
#Authors:
    #   Jonathan Poulton n9731008
def ModelNameUpdate(request):
    context_dict = {}
    make = request.GET['make']
    context_dict['modelNames'] = Car_Model.objects.filter(make=make).values('modelName').distinct()
    return render(request, 'BranchPerformanceTool/modelNameData.html', context_dict)

#This method updates the dropdown lsit for the series
#This is filtered based off the make and modelName dropdowns
#Authors:
    #   Jonathan Poulton n9731008
def ModelSeriesUpdate(request):
    context_dict = {}
    make = request.GET['make']
    modelName = request.GET['modelName']
    context_dict['seriesList'] = Car_Model.objects.filter(make=make, modelName=modelName).values('series').distinct()
    return render(request, 'BranchPerformanceTool/seriesData.html', context_dict)

def CarSummary(request):
    context_dict = {}
    loggedInStatus = User_Authentication.LoginStatus(request)
    context_dict['loggedIn'] = loggedInStatus
    #Check if the user is logged in and has staff access
    if loggedInStatus:
        accessRequired = User_Authentication.STAFF_ROLE
        if not User_Authentication.AccessLevelMet(request, accessRequired):
            return redirect('/CarRental/home')
        else:
            context_dict['userIsStaff'] = True
    else:
        return redirect('/CarRental/login')

    #Check how the page was accessed
    if request.method == 'GET':
        #Get the customer
        try:
            modelID = request.GET['modelID']
        except Exception as e:
            return redirect('/CarRental/CustomerTools/search')

        #Get the customer and send it to the html
        model = Car_Model.objects.get(modelID=modelID)
        context_dict['models'] = model 

        #Get the staff member and check their access, then add them to the dictionary

        return render(request, 'CustomerTools/CarSummaryTool.html', context_dict)
    else:
        return redirect('/CarRental/CustomerTools/search')

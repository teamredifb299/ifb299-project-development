//This method is called when the selection of the return table's pickup store dropdown
//is changed
//Author: Jonathan Poulton n9731008
$("#pickupStoreReturn").change(function(){
    ReturnTable();
});

//This is called when the return table is loaded when the page opens
//Author: Jonathan Poulton n9731008
$("#closedRentalsTable").ready(function(){
    ReturnTable();
});

//This method is called with the return tables pickupdate selection is changed
//Author: Jonathan Poulton n9731008
$("#pickupDateReturn").change(function(){
    ReturnTable();
});

//This method updates the Return Table
//Author: Jonathan Poulton n9731008
function ReturnTable(){
    //Get the URL for the view being loaded
    var url = $("#closedRentalsTable").attr("searchURL");
    //Get the search parameters
    var pickupStoreID = $("#pickupStoreReturn").val();
    var pickupDate = $("#pickupDateReturn").val();
    var customerID = $("#userID").attr("userID");
    //Send an ajax query to update the returns table without refreshing the page
    $.ajax({
        url:url,
        data:{
            'pickupStoreID': pickupStoreID,
            'customerID': customerID,
            'pickupDate': pickupDate, 
        },
        success: function(data){
            //Update the table's html
            $("#closedRentalsTable").html(data);
        }
    });
}

//This method is called when the active rentals table is loaded
//Author: Jonathan Poulton n9731008
$("#activeRentalsTable").ready(function(){
    PickupTable();
});

//This method is called when the pickup table's store dropdown is changed
//Author: Jonathan Poulton n9731008
$("#pickupStorePickup").change(function(){
    PickupTable();
});

//This method is called when the pickup table's date selection is changed
//Author: Jonathan Poulton n9731008
$("#pickupDatePickup").change(function(){
    PickupTable();
});

//This method updates the pickups table
//Author: Jonathan Poulton n9731008
function PickupTable(){
    //Get the URL of the view to update the table
    var url = $("#activeRentalsTable").attr("searchURL");
    //Get the filtering parameters
    var pickupStoreID = $("#pickupStorePickup").val();
    var pickupDate = $("#pickupDatePickup").val();
    var customerID = $("#userID").attr("userID");
    //Send an ajax query to update the table without refreshing the page
    $.ajax({
        url:url,
        data:{
            'pickupStoreID': pickupStoreID,
            'customerID': customerID,
            'pickupDate': pickupDate, 
        },
        success: function(data){
            //Update the tables html
            $("#activeRentalsTable").html(data);
        }
    });
}

//This method is called when a return rental button is clicked
//It sends an asynchronous request to return the rental selected 
//Author: Jonathan Poulton n9731008
$("#returnRental").click(function(){
    //Get the URL for the view to return the vehicle
    var url = $(this).attr("returnURL");
    //Get the rental id of the pickup selected
    var rentalID = $(this).attr("rentalID");
    //Send the ajax request
    $.ajax({
        url:url,
        data:{
            'rentalID': rentalID,
        },
        success: function(){
            //Update the pickup and return tables now that they have changed
            PickupTable();
            ReturnTable();
        }
    });
});
//This method checks when the store dropdown is changed
//It updates the summarytable and the model table
//Author: Jonathan Poulton n9731008
$("#store").change(function(){
    SummaryTable();
    Search();
});

//This is called when the time period dropdown is changed
//It will update the summary table and model table
//Author: Jonathan Poulton n9731008
$("#timePeriodSelect").change(function(){
    var store = $("#store").val()
    //Check if a store has been selected
    //If one has been then update the tables
    if (store != ""){
        SummaryTable();
        Search();
    }
});

//Update the summary table
//Author: Jonathan Poulton n9731008
function SummaryTable(){
    //Get the URL for the view being called
    var url = $("#summarySearch").attr("summaryTableURL");
    //Get the storeID and time period selected in the dropdowns
    var storeID = $("#store").val();
    var timePeriod = $("#timePeriodSelect").val();
    //Reset the dropdown for the model table filter
    $("#makeDropdown").val("");

    //Use an ajax query to update the table without refreshing the page
    $.ajax({
        url: url,
        data: {
            'storeID': storeID,
            'timePeriod': timePeriod
        },
        success: function (data){
            //Set the summary table html to be what the review returned
            $("#summaryTable").html(data);
            //Make the table visible
            $("#rentalSearch").css("visibility","visible");
        }
    });
}

//This method updates the model table
//Author: Jonathan Poulton n9731008
function Search(){
    //Get the URL of the view being loaded
    var url = $("#results").attr("search_url");
    
    //Get the search parameters from the various dropdowns
    var storeID = $("#store").val();
    var make = $("#makeDropdown").val();
    var modelName = $("#modelNameDropdown").val();
    var series = $("#seriesDropdown").val();
    var timePeriod = $("#timePeriodSelect").val();
    //Send the ajax request to update the table with refreshing the page
    $.ajax({
        url: url,
        data: {
            'storeID': storeID,
            'make': make,
            'modelName': modelName,
            'series': series,
            'timePeriod': timePeriod
        },
        success: function (data){
            $("#results").html(data);
        }
    });
};

//Update the model table and the linked dropdowns for the model
//Author: Jonathan Poulton n9731008
$("#makeDropdown").change(function(){
    Search();
    var url = $("#modelNameDropdown").attr('update_url');
    var make = $(this).val();
    $.ajax({
        url: url,
        data: {
            'make': make
        },
        success: function(data){
            //Update the model name dropdown data and set its value to be blank
            $("#modelNameDropdown").html(data);
            $("#modelNameDropdown").val("");
            //Set the value of the series dropdown to null
            $("#seriesDropdown").val("");
        }
    });
});

//Update the model table and the linked dropdowns
//Author: Jonathan Poulton n9731008
$("#modelNameDropdown").change(function(){
    Search();
    //Get the URL of the view being loaded
    var url = $("#seriesDropdown").attr('update_url');
    //Get the parameters to update the linked dropdowns
    var modelName = $(this).val();
    var make = $("#makeDropdown").val()
    $.ajax({
        url: url,
        data: {
            'modelName': modelName,
            'make': make
        },
        success: function(data){
            //Update the series dropdown and reset its value
            $("#seriesDropdown").html(data);
            $("#seriesDropdown").val("");
        }
    });
});

//Update the model table when the series dropdown list is changed
//Author: Jonathan Poulton n9731008
$("#seriesDropdown").change(function(){
    Search();
})
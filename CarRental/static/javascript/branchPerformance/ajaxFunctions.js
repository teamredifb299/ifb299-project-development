$("#store").change(function(){
    CustomerSummaryTable();
});

$("#timePeriodSelect").change(function(){
    var store = $("#store").val()
    if (store != ""){
        CustomerSummaryTable();
    }
});

function CustomerSummaryTable(){
    var url = $("#customerSummarySearch").attr("customerSummaryTableURL");
    var storeID = $("#store").val();
    var timePeriod = $("#timePeriodSelect").val();

    $.ajax({
        url: url,
        data: {
            'storeID': storeID,
            'timePeriod': timePeriod
        },
        success: function (data){
            $("#customerSummaryTable").html(data);
        }
    });
}
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'CarRental.settings')

import django
import pandas as pd
import datetime
django.setup()

from app.models import *

CUSTOMER_ROLE = 'Customer'
STAFF_ROLE = 'Staff'
MANAGER_ROLE = 'Manager'
MECHANIC_ROLE = 'Mechanic'


def populate():
	create_roles()
	tempStore, created = Store.objects.get_or_create(storeID=400, name="TempStore",address="TempAddress", city="TempCity", state="TempState",phone="TempPhone")
	staff, created = Staff.objects.get_or_create(staffName="TempStaffName", username="TempStaffUsername", role=Role.objects.get(name=STAFF_ROLE), password="TempStaffPassword", store=tempStore)
	manager, created = Staff.objects.get_or_create(staffName="TempManagerName", username="TempManagerUsername",role=Role.objects.get(name=MANAGER_ROLE), password="TempManagerPassword", store=tempStore)
	mechanic, created = Staff.objects.get_or_create(staffName="TempMechanicName", username="TempMechanicUsername",role=Role.objects.get(name=MECHANIC_ROLE), password="TempMechanicPassword", store=tempStore)
	fileName = 'Data.xlsx'
	file_data = pd.ExcelFile(fileName)
	print (file_data.sheet_names)

	storeData = file_data.parse('DataInCentralDatabase')
	for index, row in storeData.iterrows():
		try:
			pickup_store = create_store_pickup(row)
			return_store = create_store_return(row)
			car = create_car(row, return_store)
			customer = create_customer(row)
			pickup = create_pickup(row, customer, staff, car, pickup_store)
			create_return(row, pickup, staff, return_store)

		except Exception as e:
			print('Bad Line')

		print(index)

def create_return(row, pickup, staff, store):
    returnDate = str(row['Order_ReturnDate']).replace(' 00:00:00', '')
    returnDate = datetime.datetime.strptime(returnDate,'%Y%m%d').strftime('%Y-%m-%d')

    Return.objects.create(rental=pickup, returnStore=store,
        returnDate=returnDate)

def create_pickup(row, customer, staff, car, store):
    rentalID = int(row['Order_ID'])
    start = str(row['Order_CreateDate']).replace(' 00:00:00', '')
    pickup = str(row['Order_PickupDate']).replace(' 00:00:00', '')
    startDate = datetime.datetime.strptime(start,'%Y%m%d').strftime('%Y-%m-%d')
    pickupDate = datetime.datetime.strptime(pickup,'%Y%m%d').strftime('%Y-%m-%d')
    pickup, created = Pickup.objects.get_or_create(rentalID=rentalID,
        startDate=startDate, pickupDate=pickupDate, pickupStore=store,
        car=car, customer=customer, staff=staff)
    return pickup

def create_staff(row, store):
    storeName = store.name
    username =  storeName.replace(' ', '') + 'User'
    password = username
    role = Role.objects.get(name=STAFF_ROLE)
    staffName =  storeName.replace(' ', '')
    staff, created = Staff.objects.get_or_create(staffName=staffName, username=username,
        role=role, password=password, store=store)
    return staff

def create_roles():
	Role.objects.get_or_create(name=CUSTOMER_ROLE)
	Role.objects.get_or_create(name=STAFF_ROLE)
	Role.objects.get_or_create(name=MANAGER_ROLE)
	Role.objects.get_or_create(name=MECHANIC_ROLE)


def create_customer(row):
    userID = row['Customer_ID']
    try:
        customer = Customer.objects.get(
            userID=userID)
    except Exception as e:
        name = row['Customer_Name']
        dob = str(row['Customer_Brithday']).replace(' 00:00:00', '')
        phone = row['Customer_Phone']
        address = row['Customer_Addresss']
        email = name.replace(' ', '.') + '@carRental.com'
        occupation = row['Customer_Occupation']
        gender = row['Customer_Gender']
        license = 123456789
        username = name.replace(' ', '')
        password = str(userID) + username
        role = Role.objects.get(name=CUSTOMER_ROLE)

        customer = Customer.objects.create(name=name, dob=dob,
            phone=phone, address=address, email=email,
            occupation=occupation, gender=gender,license=license,
            userID=userID, password=password, username=username,role=role)

    return customer

def create_store_return(row):
    storeID = row['Order_ReturnStore']
    try:
        store= Store.objects.get(storeID=storeID)
    except Exception as e:
        name = row['Return_Store_Name'].split('_')[0]
        address = row['Return_Store_Address']
        city = row['Return_Store_City']
        state = row['Return_Store_State']
        phone = row['Return_Store_Phone']

        store = Store.objects.create(storeID=storeID,
            name=name, address=address, city=city, state=state,
            phone=phone)
    return store

def create_store_pickup(row):
    storeID = row['Order_PickupStore']

    try:
        store = Store.objects.get(storeID=storeID)
    except Exception as e:
        name = row['Pickup_Store_Name']
        address = row['Pickup_Store_Address']
        city = row['Pickup_Store_City']
        state = row['Pickup_Store_State_Name']
        phone = row['Pickup_Store_Phone']

        store = Store.objects.create(storeID=storeID,
            name=name, address=address, city=city, state=state,
            phone=phone)
    return store

def create_car(row, store):
    available='available'
    carID=row['Car_ID']

    try:
        car = Car.objects.get(carID=carID)
    except Exception as e:
        make = row['Car_MakeName']
        modelName = row['Car_Model']
        model = row['Car_Model']
        series = row['Car_Series']
        year = row['Car_SeriesYear']
        price = row['Car_PriceNew']
        engineSize = row['Car_EngineSize']
        fuelSystem = row['Car_FuelSystem']
        tankCapacity = row['Car_TankCapacity']
        power = row['Car_Power']
        seatingCapacity = row['Car_SeatingCapacity']
        standardTransmission = row['Car_StandardTransmission']
        bodyType = row['Car_BodyType']
        drive = row['Car_Drive']
        wheelBase = row['Car_Wheelbase']
        dailyRate = price/365
        model, created = Car_Model.objects.get_or_create(make=make, modelName=modelName,series=series,
            seriesYear=year, priceNew=price, engineSize=engineSize,
            fuelSystem=fuelSystem, tankCapacity=tankCapacity,
            power=power, seatingCapacity=seatingCapacity,
            standardTransmission=standardTransmission,
            bodyType=bodyType, drive=drive, wheelBase=wheelBase,
            imgSource='1', dailyRate=dailyRate)

        car = Car.objects.create(carID=carID, model=model, carStatus=available, store=store)
    return car

if __name__ == '__main__':
    populate()
